PKG_HAS_NO_SOURCES = defined

include $(DEFS)


configure : $(call tstamp_f,install,shdcpd)
install-root : $(call tstamp_f,install-root,shdcpd)

define INSTALL_ROOT_CMD
  $(IW) $(INSTxFILES) $(PKG_PKG_DIR)/*.rc        $(RINIT_DIR) $(LOG)
  $(IW) $(INSTxFILES) $(PKG_PKG_DIR)/*.conf       $(RETC_DIR) $(LOG)
endef


include $(RULES)

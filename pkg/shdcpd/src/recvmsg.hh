//#include <net/if.h>      // if_indextoname

    rc = setsockopt(sk, IPPROTO_IP, IP_PKTINFO, &sopt, sizeof(sopt));
    if (rc < 0)
	dferror(EXIT_FAILURE, errno,
		"Can't set IP socket option IP_PKTINFO");



		    struct sockaddr_in src_addr;
		    socklen_t src_addrlen = sizeof(src_addr);

		    char ancillary[64]; //FIXME: size

		    struct iovec iov =
		    {
			.iov_base = recv_pktlen.pkt,
			.iov_len  = recv_pktlen.len
		    };

		    struct msghdr msg =
		    {
			.msg_name    = &src_addr, .msg_namelen    = src_addrlen,
			.msg_iov     = &iov,      .msg_iovlen     = 1,
			.msg_control = ancillary, .msg_controllen = sizeof(ancillary)
		    };

		    ssize_t rrc = recvmsg(sk, &msg, 0);
		    if (rrc < 0)
			dferror(EXIT_SUCCESS, errno, "recvmsg failed");
		    else
		    {
			if (rrc != rsize)
			{
			    if (verbose)
				dflog(LOG_ERR,
				      "%d bytes expected but %zd received",
				      rsize, rrc);
			}
			else
			{
			    if (verbose)
				dflog(LOG_INFO,
				      "%zd bytes packet received from %s:%d",
				      rrc,
				      inet_ntoa(src_addr.sin_addr),
				          ntohs(src_addr.sin_port));

			    //+ get IP_PKTINFO
			    struct cmsghdr *cmsg;
			    struct in_pktinfo *pktinfo = NULL;

			    for (cmsg = CMSG_FIRSTHDR(&msg); cmsg != NULL;
				 cmsg = CMSG_NXTHDR  (&msg,  cmsg))
			    {
				if (cmsg->cmsg_level == IPPROTO_IP &&
				    cmsg->cmsg_type  == IP_PKTINFO)
				{
				    pktinfo = (struct in_pktinfo*) CMSG_DATA(cmsg);
				    break;
				}
			    }

			    char *ifn = NULL;
			    if (pktinfo != NULL)
			    {
				char ifname[IF_NAMESIZE];
				ifn = if_indextoname(pktinfo->ipi_ifindex, ifname);
				if (ifn == NULL)
				    dferror(EXIT_SUCCESS, errno,
					    "No interface name found for index %u",
					    pktinfo->ipi_ifindex);
				else
				    dflog(LOG_INFO, "ifname: %s", ifn);

				if (verbose)
				{
				    dflog(LOG_INFO,
					  "pktinfo: ifindex: %u, ifname: %s, "
					  "local addr: %s, dst addr: %s",
					  pktinfo->ipi_ifindex, ifn,
					  inet_ntoa(pktinfo->ipi_spec_dst),
					  inet_ntoa(pktinfo->ipi_addr)
					 );
				}
			    }
			    //- get IP_PKTINFO

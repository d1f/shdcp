#define _GNU_SOURCE

#include <shdcp0.h>
#include "shdcp0_pktlen.h"
#include "private.h"

#include <df/hostinfo.h>
#include <df/shcfg.h>
#include <df/log.h>
#include <df/error.h>
#include <df/x.h>
#include <df/system-status.h>

#include <stdio.h>
#include <stdlib.h> // EXIT_SUCCESS
#include <string.h> // basename
#include <errno.h>
#include <unistd.h> // STDOUT_FILENO, usleep
#include <poll.h>

#include <sys/ioctl.h>

#include <arpa/inet.h>   // inet_ntoa, INADDR_ANY


static int verbose = 0;
static const char *ifname = "eth0";


static void make_packet_gather_info(shdcp0_pktlen_t *pl)
{
    DF_BT_ENTRY;

    int configured = hostinfo_get_shdcp_configured();
    uint8_t shdcp_flags = 0;

    if (configured == 1)
	shdcp_flags |= SHDCP0_CONFIGURED;
    // or error - status undefined

    if (hostinfo_get_dhcp() == 1)
	shdcp_flags |= SHDCP0_DHCP;

    shdcp0_pktlen_alloc(pl, sizeof(shdcp0_pkt_t));
    shdcp0_pkt_init(pl->pkt, 0, shdcp_flags);

    hostinfo_get_eth_addr      (ifname, &pl->pkt->hwaddr);
    hostinfo_get_ip4_addr      (ifname, &pl->pkt->ipaddr);
    hostinfo_get_ip4_net_mask  (ifname, &pl->pkt->ipmask);
    hostinfo_get_ip4_bcast_addr(ifname, &pl->pkt->bcaddr);
    hostinfo_get_ip4_gw_addr   (ifname, &pl->pkt->gwaddr);

    df_shcfg_t *shdcpconf = df_shcfg_create("/etc/shdcp.conf", '=');
    if (shdcpconf != NULL)
    {
	DF_SHCFG_FOREACH(shdcpconf, kv, i)
	{
	    shdcp0_pktlen_add_string(pl, kv->key.ccPtr);
	    shdcp0_pktlen_add_string(pl, kv->val.ccPtr);
	}

	df_shcfg_delete(&shdcpconf);
    }

    char *dynstr = hostinfo_get_model_name();
    if (dynstr != NULL)
    {
	shdcp0_pktlen_add_string(pl, "model");
	shdcp0_pktlen_add_string(pl,  dynstr);
	free(dynstr);
    }

    dynstr = hostinfo_get_sensor_name();
    if (dynstr != NULL)
    {
	shdcp0_pktlen_add_string(pl, "sensor");
	shdcp0_pktlen_add_string(pl,  dynstr);
	free(dynstr);
    }

    dynstr = hostinfo_get_software_version();
    if (dynstr != NULL)
    {
	shdcp0_pktlen_add_string(pl, "sw_ver");
	shdcp0_pktlen_add_string(pl,  dynstr);
	free(dynstr);
    }

    dynstr = hostinfo_get_hardware_version();
    if (dynstr != NULL)
    {
	shdcp0_pktlen_add_string(pl, "hw_ver");
	shdcp0_pktlen_add_string(pl,  dynstr);
	free(dynstr);
    }

    dynstr = hostinfo_get_host_name();
    if (dynstr != NULL)
    {
	shdcp0_pktlen_add_string(pl, "hostname");
	shdcp0_pktlen_add_string(pl,  dynstr);
	free(dynstr);
    }

    dynstr = hostinfo_get_domain_name();
    if (dynstr != NULL)
    {
	shdcp0_pktlen_add_string(pl, "domain");
	shdcp0_pktlen_add_string(pl,  dynstr);
	free(dynstr);
    }
}

static shdcp0_pktlen_t recv_pktlen = SHDCP0_PKTLEN_INIT;
static shdcp0_pktlen_t resp_pktlen = SHDCP0_PKTLEN_INIT;
static shdcp0_pktlen_t anno_pktlen = SHDCP0_PKTLEN_INIT;


static void make_announce_response_packets(void)
{
    DF_BT_ENTRY;

    if (resp_pktlen.pkt == NULL)
    {
	make_packet_gather_info(&resp_pktlen);
	resp_pktlen.pkt->command = SHDCP0_DISCOVERY_RESPONSE;

	shdcp0_pktlen_copy(&anno_pktlen, &resp_pktlen);
	anno_pktlen.pkt->command = SHDCP0_ANNOUNCE;
    }
}

static int send_pkt(int sk, const shdcp0_pktlen_t *pl)
{
    DF_BT_ENTRY;

    return shdcp_send(sk, pl->pkt, pl->len);
}

static void ip4_kv_replace(df_shcfg_t *knetconf, const char *fname,
			   const char *key, df_ip4addr_t addr)
{
    DF_BT_ENTRY;

    df_kv_t *kv = df_shcfg_bsearch(knetconf, key);
    if (kv == NULL)
    {
	dflog(LOG_INFO, "No %s key found in %s, creating", key, fname);

	df_shcfg_add(knetconf, key, NULL);
	kv = df_shcfg_bsearch(knetconf, key);
    }

    char ip4out[DF_IP4ADDR_OUT_SIZE];
    df_ip4addr_sprintf(ip4out, addr, DF_IP4ADDR_NO_ALIGN);
    free(kv->val.cPtr);
    kv->val.cPtr = df_xstrdup(ip4out);
}

static void process_packet(int sk, const shdcp0_pkt_t *pkt, size_t pkt_len)
{
    DF_BT_ENTRY;

    if (shdcp0_pkt_check(pkt, pkt_len) != 0)
	return;

    if (verbose)
	shdcp0_pkt_print(pkt, pkt_len);

    make_announce_response_packets();

    switch (pkt->command)
    {
	case SHDCP0_ANNOUNCE: // nothing to do for device
	    if (verbose)
		dflog(LOG_INFO, "ANNOUNCE packet received, ignored");
	    break;
	case SHDCP0_DISCOVERY_RESPONSE:
	    if (verbose)
		dflog(LOG_INFO, "DISCOVERY_RESPONSE packet received, ignored");
	    break;

	case SHDCP0_DISCOVERY_REQUEST:
	    if (verbose)
		dflog(LOG_INFO, "DISCOVERY_REQUEST packet received");

		uint64_t rcv_hwaddr = be64toh(            pkt->hwaddr) & ETHADDR_MASK;
		uint64_t our_hwaddr = be64toh(resp_pktlen.pkt->hwaddr) & ETHADDR_MASK;

		if (rcv_hwaddr == ETHADDR_ANY || rcv_hwaddr == ETHADDR_BROADCAST)
		    ;
		else if (rcv_hwaddr != our_hwaddr) // select on ethernet address
		{
		    if (verbose)
			dflog(LOG_INFO, "ethernet address is not our, ignore");
		    break;
		}

		if (verbose)
		    dflog(LOG_INFO, "selected, send response ...");

		send_pkt(sk, &resp_pktlen);
	    break;
	case SHDCP0_CONFIGURE:
	    if (verbose)
		dflog(LOG_INFO, "CONFIGURE packet received");

	    // select on ethernet address
	    if ((be64toh(            pkt->hwaddr) & ETHADDR_MASK) !=
		(be64toh(resp_pktlen.pkt->hwaddr) & ETHADDR_MASK))
	    {
		if (verbose)
		    dflog(LOG_INFO, "ethernet address is not our, ignore");
		break;
	    }

	    if (resp_pktlen.pkt->flags & SHDCP0_CONFIGURED)
	    {
		if (verbose)
		    dflog(LOG_INFO, "already configured, ignore");
		break;
	    }


	    dflog(LOG_INFO, "Configuring ...");
	    size_t configure_failed = 0;

	    const char *fnetconf = "/etc/network.conf";
	    df_shcfg_t *knetconf = df_shcfg_create(fnetconf, '=');
	    if (knetconf == NULL || df_shcfg_length(knetconf) == 0)
	    {
		df_shcfg_delete(&knetconf);
		fnetconf = "/etc/network_wired.conf";
		knetconf = df_shcfg_create(fnetconf, '=');
	    }
	    if (knetconf == NULL || df_shcfg_length(knetconf) == 0)
		df_shcfg_delete(&knetconf);

	    size_t netconf_replaced = 0;

	    if (be32toh(pkt->ipaddr) != INADDR_ANY)
	    {
		ip4_kv_replace(knetconf, fnetconf, "address", pkt->ipaddr);
		++netconf_replaced;
	    }

	    if (be32toh(pkt->ipmask) != INADDR_ANY)
	    {
		ip4_kv_replace(knetconf, fnetconf, "netmask", pkt->ipmask);
		++netconf_replaced;
	    }

	    if (be32toh(pkt->gwaddr) != INADDR_ANY)
	    {
		ip4_kv_replace(knetconf, fnetconf, "gateway", pkt->gwaddr);
		++netconf_replaced;
	    }

	    if (be32toh(pkt->bcaddr) != INADDR_ANY)
	    {
		ip4_kv_replace(knetconf, fnetconf, "broadcast", pkt->bcaddr);
		++netconf_replaced;
	    }

	    {
		const char *dhcp = pkt->flags & SHDCP0_DHCP ? "on" : "off";

		df_kv_t *kv = df_shcfg_bsearch(knetconf, "dhcp");

		if (kv == NULL)
		{
		    dflog(LOG_INFO, "No %s key found in %s, creating", "dhcp", fnetconf);
		    df_shcfg_add(knetconf, "dhcp", dhcp);
		}
		else
		{
		    free(kv->val.cPtr);
		    kv->val.cPtr = df_xstrdup(dhcp);
		}
		++netconf_replaced;
	    }

	    if (netconf_replaced)
	    {
		if (df_shcfg_write(knetconf, fnetconf, '=', '"') != 0)
		    ++configure_failed;
	    }

	    df_shcfg_delete(&knetconf);

	    int rc = system("/init/network restart >/dev/null 2>&1");
	    if ( df_system_status("network restart", rc) < 0)
		++configure_failed;

	    if (configure_failed)
	    {
		dflog(LOG_INFO, "Configure failed.");
	    }
	    else
	    {
		hostinfo_set_shdcp_configured();
		dflog(LOG_INFO, "Configure succeed.");
	    }

	    break;

	default:
	    dflog(LOG_ERR, "Unknown command %d", pkt->command);
    }
}

static void on_sighup(int sk)
{
    DF_BT_ENTRY;

    // clean all packets:
    shdcp0_pktlen_free(&anno_pktlen);
    shdcp0_pktlen_free(&recv_pktlen);
    shdcp0_pktlen_free(&resp_pktlen);

    make_announce_response_packets(); // gather info again

    pid_t child = fork();
    if (child == -1)
	dferror(EXIT_FAILURE, errno, "fork failed");

    if (child == 0)
    {
	size_t n;
	for (n = SHDCP0_SEND_REPEAT_COUNT - 1; n; --n)
	{
	    send_pkt(sk, &anno_pktlen);
	    usleep(SHDCP0_SEND_REPEAT_DELAY_USEC);
	}
	send_pkt(sk, &anno_pktlen);

	exit(EXIT_SUCCESS);
    }
}


static sig_atomic_t stop = 0;
static sig_atomic_t hup  = 0;

static void sig_handler(int sig)
{
    DF_BT_ENTRY;

    if (sig == SIGHUP)
	hup = sig;
    else
	stop = sig;

    dflog(LOG_INFO, "%s signal catched", strsignal(sig));
}

int main(int ac, char *av[])
{
    DF_BT_ENTRY;

    dflog_open(basename(av[0]), DFLOG_PID | DFLOG_SYS | DFLOG_STDERR);

    if (ac < 2 || ac > 3)
    {
	dflog(LOG_ERR, "parameters: iface [-v]");
	return EXIT_FAILURE;
    }

    ifname = av[1];
    if (ac >= 3 && strcmp(av[2], "-v") == 0)
	verbose = 1;


    struct sigaction sigact;
    memset(&sigact, 0, sizeof(sigact));
    sigact.sa_handler = sig_handler;
    sigact.sa_flags   = SA_RESTART;

    signal(SIGCHLD, SIG_IGN);
    signal(SIGPIPE, SIG_IGN);
    df_xsigaction(SIGHUP , &sigact);
    df_xsigaction(SIGINT , &sigact);
    df_xsigaction(SIGQUIT, &sigact);
    df_xsigaction(SIGTERM, &sigact);


    int sk = shdcp_socket(ifname, 1);

    if (daemon(0, 0) < 0)
	dferror(EXIT_FAILURE, errno, "Can't daemonize");

    dflog_open(basename(av[0]), DFLOG_PID | DFLOG_SYS);
    dflog(LOG_INFO, "started");


    on_sighup(sk); // send announce


    struct pollfd pollfd = { .fd = sk, .events = POLLIN };
    while ( !stop )
    {
	if (hup)
	{
	hupcheck:
	    hup = 0;
	    on_sighup(sk); // reconfigure and send updated announce
	    continue;
	}

	int rc = poll(&pollfd, 1, -1);
	if (rc < 0)
	{
	    if (errno == EINTR)
	    {
		if (hup)
		    goto hupcheck;
		break;
	    }
	    dferror(EXIT_FAILURE, errno, "poll failed");
	}
	else if (rc == 0) // timeout
	    dferror(EXIT_FAILURE, errno, "poll timeout");
	else if (rc == 1)
	{
	    if (pollfd.revents & POLLIN)
	    {
		int rsize = 0;
		int err = ioctl(sk, FIONREAD, &rsize);
		if (err < 0)
		    dferror(EXIT_SUCCESS, errno, "ioctl FIONREAD failed");
		else // err >= 0
		{
		    if ((size_t)rsize > recv_pktlen.len)
			shdcp0_pktlen_alloc(&recv_pktlen, rsize);

		    struct sockaddr_in src_addr;
		    socklen_t src_addrlen = sizeof(src_addr);
		    ssize_t rrc = recvfrom(sk, recv_pktlen.pkt, recv_pktlen.len,
					   0, &src_addr, &src_addrlen);
		    if (rrc < 0)
			dferror(EXIT_SUCCESS, errno, "recvfrom failed");
		    else
		    {
			if (rrc != rsize)
			{
			    if (verbose)
				dflog(LOG_ERR,
				      "%d bytes expected but %zd received",
				      rsize, rrc);
			}
			else
			{
			    if (verbose)
				dflog(LOG_INFO,
				      "%zd bytes packet received from %s:%d",
				      rrc,
				      inet_ntoa(src_addr.sin_addr),
				          ntohs(src_addr.sin_port));

			    process_packet(sk, recv_pktlen.pkt, rrc);
			}
		    }
		}
	    }
	    else
	    {
		dflog(LOG_ERR, "No events on poll return!");
	    }
	}
	else
	    dferror(EXIT_FAILURE, errno, "poll returned %d, 1 expected", rc);
    }

    dflog(LOG_INFO, "stopped by %s signal", strsignal(stop));
    close(sk);
    dflog_close();
    return EXIT_SUCCESS;
}

#define _GNU_SOURCE
#include "private.h"
#include <stddef.h> // offsetof
#include <string.h> // strnlen
#include <df/log.h>
#include <df/netaddr.h>


static void print_ip4(const char *pfx, df_ip4addr_t ipaddr)
{
    DF_BT_ENTRY;

    char buf[DF_IP4ADDR_OUT_SIZE];
    df_ip4addr_sprintf(buf, ipaddr, DF_IP4ADDR_NO_ALIGN);
    dflog(LOG_INFO, "%s: %s", pfx, buf);
}

void shdcp0_pkt_print(const shdcp0_pkt_t *pkt, size_t pktlen)
{
    DF_BT_ENTRY;

    dflog_("Command: ");
    switch (pkt->command)
    {
	case SHDCP0_DISCOVERY_REQUEST:
	    dflog(LOG_INFO, "DISCOVERY_REQUEST");
	    break;
	case SHDCP0_DISCOVERY_RESPONSE:
	    dflog(LOG_INFO, "DISCOVERY_RESPONSE");
	    break;
	case SHDCP0_ANNOUNCE:
	    dflog(LOG_INFO, "ANNOUNCE");
	    break;
	case SHDCP0_CONFIGURE:
	    dflog(LOG_INFO, "CONFIGURE");
	    break;
	default:
	    dflog(LOG_INFO, "Unknown %d", (int)pkt->command);
	    break;
    }

    size_t flags_count = 0;
    dflog_("Flags: ");
    if (pkt->flags & SHDCP0_CONFIGURED)
    {
	dflog_("CONFIGURED");
	++flags_count;
    }
    if (pkt->flags & SHDCP0_DHCP)
    {
	if (flags_count)
	    dflog_(", ");
	dflog_("DHCP");
	++flags_count;
    }
    dflog_flush(LOG_INFO);


    char buf[DF_ETHADDR_OUT_SIZE];
    df_ethaddr_sprintf(buf, pkt->hwaddr);
    dflog(LOG_INFO, "hwaddr: %s", buf);

    print_ip4("ipaddr", pkt->ipaddr);
    print_ip4("ipmask", pkt->ipmask);
    print_ip4("bcaddr", pkt->bcaddr);
    print_ip4("gwaddr", pkt->gwaddr);


    const char *s = (const char *) pkt->strings;

    size_t maxlen = pktlen - offsetof(shdcp0_pkt_t, strings);

    size_t snum;
    for (snum=0; ; ++snum)
    {
	size_t slen = strnlen(s, maxlen);
	if (slen < maxlen)
	{
	    dflog(LOG_INFO, "strings[%zu]: '%s'", snum, s);
	    s      += (slen + 1);
	    maxlen -= (slen + 1);
	}
	else
	    break;
    }

    dflog(LOG_INFO, " ");
}

#ifndef  _SHDCP0_PKT_
# define _SHDCP0_PKT_

# include <shdcp0.h>
# include <stdlib.h>  // size_t
# include <df/defs.h> // ATTR


void shdcp0_pkt_print(const shdcp0_pkt_t *pkt, size_t pktlen) ATTR(nonnull);


#endif //_SHDCP0_PKT_

#include <shdcp0_pktlen.h>
#include <string.h>
#include <df/x.h>

void shdcp0_pktlen_add_string(shdcp0_pktlen_t *pl, const char *str)
{
    DF_BT_ENTRY;

    size_t new_pkt_len = pl->len + strlen(str) + 1;
    pl->pkt = df_xrealloc(pl->pkt, new_pkt_len);
    strcpy((char*)pl->pkt + pl->len, str);
    pl->len = new_pkt_len;
}

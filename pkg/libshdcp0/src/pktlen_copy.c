#include <shdcp0_pktlen.h>
#include <df/x.h>

void shdcp0_pktlen_copy(shdcp0_pktlen_t *dst, const shdcp0_pktlen_t *src)
{
    DF_BT_ENTRY;

    shdcp0_pktlen_free(dst);
    dst->pkt = df_xmemdup(src->pkt, src->len);
    dst->len = src->len;
}

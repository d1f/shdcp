#include <shdcp0.h>
#include <string.h>     // memset
#include <df/error.h>
#include <errno.h>
#include <unistd.h>     // geteuid
#include <arpa/inet.h>  // htonX
#include <sys/socket.h> // setsockopt, bind


// create and set socket.
// SO_BINDTODEVICE if ifname; bind() if do_bind;
// returns socket.
int shdcp_socket(const char *ifname, int do_bind)
{
    DF_BT_ENTRY;

    int sk = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (sk < 0)
	dferror(EXIT_FAILURE, errno, "Can't create UDP socket");

    int rc = 0;
    int sopt = 1;

    if (ifname != NULL)
    {
	// shdcpd only
	if (geteuid() == 0) // root
	{
	    rc = setsockopt(sk, SOL_SOCKET, SO_BINDTODEVICE,
			    ifname, strlen(ifname)+1);
	    if (rc < 0)
		dferror(EXIT_FAILURE, errno,
			"Can't bind socket to %s", ifname);
	}
    }


    sopt = 1;
    rc = setsockopt(sk, SOL_SOCKET, SO_BROADCAST, &sopt, sizeof(sopt));
    if (rc < 0)
	dferror(EXIT_FAILURE, errno,
		"Can't set socket option SO_BROADCAST");


    sopt = 1;
    rc = setsockopt(sk, SOL_IP, IP_TTL, &sopt, sizeof(sopt));
    if (rc < 0)
	dferror(EXIT_FAILURE, errno, "Can't set TTL to 1");

    sopt = 1;
    rc = setsockopt(sk, SOL_SOCKET, SO_REUSEADDR, &sopt, sizeof(sopt));
    if (rc < 0)
        dferror(EXIT_FAILURE, errno, "Can't set socket option SO_REUSEADDR");


    if (do_bind)
    {
	struct sockaddr_in sa;
	memset(&sa, 0, sizeof(sa));
	sa.sin_family      = AF_INET;
	sa.sin_port        = htons(SHDCP0_PORT);
	sa.sin_addr.s_addr = htonl(INADDR_ANY);
	rc = bind(sk, (const struct sockaddr *)&sa, sizeof(sa));
	if (rc < 0)
	    dferror(EXIT_FAILURE, errno, "Can't bind UDP socket to port %hd",
		    SHDCP0_PORT);
    }


    return sk;
}

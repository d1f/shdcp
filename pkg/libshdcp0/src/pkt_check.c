#define _GNU_SOURCE
#include <shdcp0.h>
#include <string.h> // memcmp
#include <stddef.h> // offsetof
#include <df/log.h>


int shdcp0_pkt_check(const shdcp0_pkt_t *pkt, size_t pktlen)
{
    DF_BT_ENTRY;

    if (pktlen < offsetof(shdcp0_pkt_t, strings))
    {
	dflog(LOG_ERR, "Packet length %zu is lesser then %zu",
	      pktlen, offsetof(shdcp0_pkt_t, strings));
	return -1;
    }

    if (memcmp(pkt->signature,
	       SHDCP0_SIGNATURE_STRING, SHDCP0_SIGNATURE_LENGTH) != 0)
    {
	dflog(LOG_INFO, "Protocol signature did not matched");
	return -1;
    }

    if (pkt->version != SHDCP0_VERSION)
    {
	dflog(LOG_INFO, "Protocol version %d does not supported", (int)pkt->version);
	return -1;
    }

#if 0
    if (pkt->flags & ~SHDCP0_ALL_FLAGS)
    {
	dflog(LOG_INFO, "Unknown flags: 0x%02x", (int)pkt->flags);
	return -1;
    }
#endif

    return 0;
}

#define _GNU_SOURCE
#include <shdcp0.h>
#include <string.h> // memcmp

int shdcp0_pkt_cmp(const shdcp0_pkt_t *pkt0, size_t pktlen0,
		   const shdcp0_pkt_t *pkt1, size_t pktlen1)
{
    DF_BT_ENTRY;

    if (pktlen0 != pktlen1)
	return 1;

    return memcmp(pkt0, pkt1, pktlen0);
}

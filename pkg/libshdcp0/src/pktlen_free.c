#include <shdcp0_pktlen.h>

void shdcp0_pktlen_free(shdcp0_pktlen_t *pl)
{
    DF_BT_ENTRY;

    free(pl->pkt);
    shdcp0_pktlen_init(pl);
}

#include <shdcp0.h>
#include <string.h>     // memset
#include <df/error.h>
#include <errno.h>
#include <arpa/inet.h>  // htonX
#include <sys/socket.h> // sendto

// returns 0 on success, -1 on error; message reported.
int shdcp_send(int sk, void *data, size_t len)
{
    DF_BT_ENTRY;

    struct sockaddr_in sa;
    memset(&sa, 0, sizeof(sa));
    sa.sin_family      = AF_INET;
    sa.sin_port        = htons(SHDCP0_PORT);
    sa.sin_addr.s_addr = htonl(INADDR_BROADCAST);

    ssize_t rc = sendto(sk, data, len, 0,
			(const struct sockaddr *)&sa, sizeof(sa));
    if (rc < 0)
	dferror(EXIT_SUCCESS, errno, "Can't send UDP packet");

    return rc >= 0 ? 0 : rc;
}

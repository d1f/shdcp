#ifndef  _SHDCP0_PKTLEN_
# define _SHDCP0_PKTLEN_

# include <shdcp0.h>
# include <stdlib.h>  // size_t


typedef struct
{
    shdcp0_pkt_t *pkt;
    size_t len;
} shdcp0_pktlen_t;

# define SHDCP0_PKTLEN_INIT { .pkt = NULL, .len = 0 }


void shdcp0_pktlen_init      (shdcp0_pktlen_t *pl)                              ATTR(nonnull);
void shdcp0_pktlen_alloc     (shdcp0_pktlen_t *pl, size_t pktlen)               ATTR(nonnull);
void shdcp0_pktlen_free      (shdcp0_pktlen_t *pl)                              ATTR(nonnull);
void shdcp0_pktlen_copy      (shdcp0_pktlen_t *dst, const shdcp0_pktlen_t *src) ATTR(nonnull);
void shdcp0_pktlen_add_string(shdcp0_pktlen_t *pl, const char *str)             ATTR(nonnull);


#endif //_SHDCP0_PKTLEN_

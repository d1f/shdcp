#include <shdcp0_pktlen.h>

void shdcp0_pktlen_init(shdcp0_pktlen_t *pl)
{
    DF_BT_ENTRY;

    pl->pkt = NULL;
    pl->len = 0;
}

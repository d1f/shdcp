# include <shdcp0.h>
# include <string.h> // memset memcpy


void shdcp0_pkt_init(shdcp0_pkt_t *pkt, uint8_t cmd, uint8_t flags)
{
    DF_BT_ENTRY;

    memset(pkt, 0, sizeof(*pkt));
    memcpy(pkt, SHDCP0_SIGNATURE_STRING, SHDCP0_SIGNATURE_LENGTH);
    pkt->version = SHDCP0_VERSION;
    pkt->command = cmd;
    pkt->flags   = flags;
}

#ifndef  _SHDCP0_H_
# define _SHDCP0_H_

# include <stdint.h>  // uintXX_t
# include <stdlib.h>  // size_t
# include <df/defs.h> // ATTR
# include <df/bt.h>


enum SHDCP0_CMD
{
    SHDCP0_ANNOUNCE          ,  // hosts -> control points (like RESPONSE)
    SHDCP0_DISCOVERY_REQUEST ,  // control points -> hosts
    SHDCP0_DISCOVERY_RESPONSE,  // hosts -> control point
    SHDCP0_CONFIGURE            // control point -> host
};

enum SHDCP0_FLAGS
{
    SHDCP0_CONFIGURED   = 1,
    SHDCP0_DHCP         = 2,
};

# define SHDCP0_ALL_FLAGS (SHDCP0_CONFIGURED | SHDCP0_DHCP)

# define SHDCP0_SIGNATURE_STRING "SHDCP"
# define SHDCP0_SIGNATURE_INIT { 'S','H','D','C','P' }
# define SHDCP0_SIGNATURE_LENGTH 5
# define SHDCP0_VERSION 0

typedef struct
{
    uint8_t signature[SHDCP0_SIGNATURE_LENGTH];  // 'SHDCP'
    uint8_t version;		// 0
    uint8_t command;		// enum SHDCP0_CMD
    uint8_t flags;		// enum SHDCP0_FLAGS

    uint64_t  hwaddr;		// Hardware (Ethernet) address in low 48 bits
    uint32_t  ipaddr;		// IP address
    uint32_t  ipmask;		// IP network mask
    uint32_t  bcaddr;		// broadcast address
    uint32_t  gwaddr;		// gateway address

    uint8_t strings[0];		// strings area start

} shdcp0_pkt_t;


# define SHDCP0_PORT 29166 // FIXME: temporarely from IANA Unassigned

# define SHDCP0_SEND_REPEAT_COUNT 4
# define SHDCP0_SEND_REPEAT_DELAY_USEC (500*1000) // 0.5 sec


// some useful routines
void shdcp0_pkt_init (      shdcp0_pkt_t *pkt, uint8_t cmd, uint8_t flags) ATTR(nonnull);
int  shdcp0_pkt_check(const shdcp0_pkt_t *pkt , size_t pktlen)             ATTR(nonnull);
int  shdcp0_pkt_cmp  (const shdcp0_pkt_t *pkt0, size_t pktlen0,
		      const shdcp0_pkt_t *pkt1, size_t pktlen1)            ATTR(nonnull);

// create and set socket.
// SO_BINDTODEVICE if ifname; bind() if do_bind;
// returns socket.
int shdcp_socket(const char *ifname, int do_bind);

// returns 0 on success, -1 on error; message reported.
int shdcp_send(int sk, void *data, size_t len)                             ATTR(nonnull);


#endif //_SHDCP0_H_

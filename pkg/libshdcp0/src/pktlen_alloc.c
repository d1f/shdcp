#include <shdcp0_pktlen.h>
#include <df/x.h>

void shdcp0_pktlen_alloc(shdcp0_pktlen_t *pl, size_t pktlen)
{
    DF_BT_ENTRY;

    shdcp0_pktlen_free(pl);
    pl->len = pktlen < sizeof(shdcp0_pkt_t) ? sizeof(shdcp0_pkt_t) : pktlen;
    pl->pkt = df_xzmalloc(pl->len);
}

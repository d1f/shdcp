#define _GNU_SOURCE

#include <shdcp0.h>
#include <shdcp0_pktlen.h>

#include <df/netaddr.h>
#include <df/log.h>
#include <df/error.h>
#include <df/x.h>
#include <df/opt.h>
#include <df/key-val.h>

#include <stdio.h>
#include <stdlib.h> // EXIT_SUCCESS
#include <string.h> // basename


static int usage(const dfopt_t opts[], const char *av0)
{
    DF_BT_ENTRY;

    printf("Usage: %s command [options] [-- [strings ...]]\n"
	   "  command: { req | resp | ann | conf }\n"
	   , basename(av0));
    printf("  options:\n");
    dfopt_print(opts, stdout);
    exit(EXIT_SUCCESS);
}

static const df_key_val_t cmd_vals[] =
{
    { .key = "req" , .val = SHDCP0_DISCOVERY_REQUEST  },
    { .key = "resp", .val = SHDCP0_DISCOVERY_RESPONSE },
    { .key = "ann" , .val = SHDCP0_ANNOUNCE           },
    { .key = "conf", .val = SHDCP0_CONFIGURE          },
    { .key = NULL },
};

static int shdcp_flag_conf   = 0;
static int shdcp_flag_dhcp   = 0;

static int parse_ethaddr(const dfopt_t *opt, const char *value)
{
    DF_BT_ENTRY;

    if ( df_ethaddr_sscanf(value, opt->data) )
	dferror(EXIT_FAILURE,0, "%s is not recognized", value);

    return 0;
}

static int parse_ipv4addr(const dfopt_t *opt, const char *value)
{
    DF_BT_ENTRY;

    if ( df_ip4addr_sscanf(value, opt->data) )
	dferror(EXIT_FAILURE,0, "%s is not recognized", value);

    return 0;
}

int main(int ac, char *av[])
{
    DF_BT_ENTRY;

    dflog_open(basename(av[0]), DFLOG_STDERR);

    static int help_flag = 0;
    static df_ip4addr_t ipaddr, ipmask, bcaddr, gwaddr;
    static df_ethaddr_t hwaddr;
    static const dfopt_t opts[] =
    {
	{ .key = "help", .desc = "this text", .int_ptr = &help_flag, .int_val = 1 },
	{
	    .key = "conf", .desc = "CONFIGURED flag",
	    .int_ptr = &shdcp_flag_conf, .int_val = SHDCP0_CONFIGURED
	},
	{
	    .key = "dhcp", .desc = "DHCP flag",
	    .int_ptr = &shdcp_flag_dhcp, .int_val = SHDCP0_DHCP
	},
	{
	    .key = "hw", .desc = "MAC: hardware Ethernet/MAC address, xx:xx:xx:xx:xx:xx",
	    .has_value = 1, .func = parse_ethaddr , .data = &hwaddr
	},
	{
	    .key = "ip", .desc = "addr: IPv4 address, ddd.ddd.ddd.ddd",
	    .has_value = 1, .func = parse_ipv4addr, .data = &ipaddr
	},
	{
	    .key = "mask", .desc = "mask: network mask",
	    .has_value = 1, .func = parse_ipv4addr, .data = &ipmask
	},
	{
	    .key = "bc", .desc = "addr: broadcast address",
	    .has_value = 1, .func = parse_ipv4addr, .data = &bcaddr
	},
	{
	    .key = "gw", .desc = "addr: gateway address",
	    .has_value = 1, .func = parse_ipv4addr, .data = &gwaddr
	},
	{ .key = NULL }
    };

    size_t after_opts_idx = dfopt_parse(opts, ac, av);

    if (help_flag || ac < 2)
	usage(opts, av[0]);

    const char *cmd_str   = av[1];

    const df_key_val_t *kv = df_key_val_lsearch(cmd_vals, cmd_str);
    if (kv == NULL)
	dferror(EXIT_FAILURE, 0, "Invalid command: %s", cmd_str);
    enum SHDCP0_CMD cmd = kv->val;

    shdcp0_pktlen_t pl = SHDCP0_PKTLEN_INIT;
    shdcp0_pktlen_alloc(&pl, sizeof(shdcp0_pkt_t));
    shdcp0_pkt_init(pl.pkt, cmd, shdcp_flag_conf | shdcp_flag_dhcp);

    pl.pkt->hwaddr = hwaddr;
    pl.pkt->ipaddr = ipaddr;
    pl.pkt->ipmask = ipmask;
    pl.pkt->bcaddr = bcaddr;
    pl.pkt->gwaddr = gwaddr;

    if ((size_t)ac > after_opts_idx) // args after --
	for (int i = after_opts_idx; i < ac; ++i) // append strings to packet
	    shdcp0_pktlen_add_string(&pl, av[i]);

    int sk = shdcp_socket(NULL, 0);
    shdcp_send(sk, pl.pkt, pl.len);

    return EXIT_SUCCESS;
}

#define _GNU_SOURCE

#include <shdcp0.h>
#include <shdcp0_pktlen.h>

#include <df/log.h>
#include <df/error.h>
#include <df/netaddr.h>
#include <df/x.h>
#include <df/da.h>
#include <df/tty.h>
#include <termios.h>


#include <stdio.h>
#include <stdlib.h> // EXIT_SUCCESS
#include <string.h> // basename
#include <signal.h>
#include <errno.h>
#include <unistd.h> // STDOUT_FILENO, usleep
#include <poll.h>

#include <sys/ioctl.h>

#include <arpa/inet.h>   // inet_ntoa


static shdcp0_pktlen_t recv_pktlen = SHDCP0_PKTLEN_INIT;

#if 0 // unused
static void ip4print(df_ip4addr_t ip4addr)
{
    DF_BT_ENTRY;

    char buf[DF_IP4ADDR_OUT_SIZE];
    df_ip4addr_sprintf(buf, ip4addr, DF_IP4ADDR_ALIGN_WHOLE);
    fputs(buf, stdout);
}
#endif

static void ip4maskprint(df_ip4addr_t addr, df_ip4addr_t mask)
{
    DF_BT_ENTRY;

    char buf[DF_IP4ADDR_OUT_SIZE];
    df_ip4addr_sprintf(buf, addr, DF_IP4ADDR_NO_ALIGN);
    size_t len = strlen(buf);
    fputs(buf, stdout);
    len += printf("/%d", df_ip4addr_mask2pfxlen(mask));
    buf[0] = '\0';
    size_t i;
    for (i=len; i < DF_IP4ADDR_OUT_SIZE-1+3; ++i)
	strcat(buf, " ");
    fputs(buf, stdout);
}

static df_da_t *plarray;

static size_t print_strings(const shdcp0_pktlen_t *pl)
{
    DF_BT_ENTRY;

    const char *s = (const char *) pl->pkt->strings;

    size_t maxlen = pl->len - offsetof(shdcp0_pkt_t, strings);

    size_t outlen = 0;

    size_t snum;
    for (snum=0; ; ++snum)
    {
	size_t slen = strnlen(s, maxlen);
	if (slen < maxlen)
	{
	    fputs(" ", stdout);
	    outlen += 1;

	    fputs(s, stdout);
	    outlen += slen;

	    s      += (slen + 1);
	    maxlen -= (slen + 1);
	}
	else
	    break;
    }

    return outlen;
}

static void display_packet(const shdcp0_pktlen_t *pl)
{
    DF_BT_ENTRY;

    df_ethaddr_fprintf(stdout, pl->pkt->hwaddr);
    printf(" ");

    if (pl->pkt->flags & SHDCP0_CONFIGURED)
	printf("C");
    else
	printf("-");

    printf(" ");

    if (pl->pkt->flags & SHDCP0_DHCP)
	printf("D");
    else
	printf("-");

    printf(" ");

    ip4maskprint(pl->pkt->ipaddr, pl->pkt->ipmask);
    //printf(" ");
    //ip4print(pl->pkt->bcaddr);
    //printf(" ");
    //ip4print(pl->pkt->gwaddr);

    print_strings(pl);

    printf("\n");
}

static void process_packet(int sk, const shdcp0_pkt_t *pkt, size_t pkt_len)
{
    DF_BT_ENTRY;

    (void)sk;
    if (shdcp0_pkt_check(pkt, pkt_len) != 0)
	return;

    //FIXME: make_announce_response_packets();

    int display = 0;
    shdcp0_pktlen_t *pl = NULL;

    switch (pkt->command)
    {
	case SHDCP0_CONFIGURE:
	case SHDCP0_DISCOVERY_REQUEST: // nothing to do for client
	    break;

	case SHDCP0_ANNOUNCE:
	case SHDCP0_DISCOVERY_RESPONSE:
	    display = 0;

	    shdcp0_pktlen_t plsearch;
	    shdcp0_pktlen_init(&plsearch);
	    plsearch.pkt->hwaddr = pkt->hwaddr;
	    pl = df_da_bsearch_item(plarray, &plsearch);

	    if (pl == NULL) // new address
	    {
		pl = df_xmalloc(sizeof(*pl));
		shdcp0_pktlen_init (pl);
		shdcp0_pktlen_alloc(pl, pkt_len);
		memcpy(pl->pkt, pkt, pkt_len);
		df_da_append_item(plarray, pl);
		df_freep(pl);
		display = 1;
	    }
	    else // existing packet
	    {
		if ( shdcp0_pkt_cmp(    pkt, pkt_len,
				    pl->pkt, pl->len))
		{
		    display = 1;
		    const shdcp0_pktlen_t src = { .pkt = pkt, .len = pkt_len };
		    shdcp0_pktlen_copy(pl, &src); // replace
		}
	    }

	    if (display)
		display_packet(pl);

	    break;

	default:
	    dflog(LOG_ERR, "Unknown command %d", pkt->command);
    }
}

static void send_discovery_request(int sk)
{
    DF_BT_ENTRY;

    shdcp0_pkt_t pkt;
    shdcp0_pkt_init(&pkt, SHDCP0_DISCOVERY_REQUEST, 0);
    shdcp_send(sk, &pkt, sizeof(pkt));
}


static int stop = 0;

static void sig_handler(int sig)
{
    DF_BT_ENTRY;

    (void)sig;
    stop = 1;
}

static struct termios saved_tattr;

static void onexit(int n, void* data)
{
    DF_BT_ENTRY;

    (void)n; (void)data;
    df_tty_restore_attr(STDIN_FILENO, &saved_tattr);
}

static void print_headers(void)
{
    DF_BT_ENTRY;

#if 0
    printf("    ethernet        IP  address         broadcast        gateway    \n");
    printf("----------------- ------------------ --------------- ---------------\n");
#endif
    printf("\n");
    printf("c - Clean table; d - Clean and Discovery Request; h - headers; q - Quit\n");
    printf("\n");
    printf("                  +-CONFIGURED\n");
    printf("                  | +-DHCP\n");
    printf("    ethernet      C D  IP  address       strings\n");
    printf("----------------- - - ------------------ --------------------------------------\n");
}

static void pl_dtor(void *pl)
{
    DF_BT_ENTRY;

    shdcp0_pktlen_free((shdcp0_pktlen_t*)pl);
}

static int pl_cmp(const void *l, const void *r)
{
    DF_BT_ENTRY;

    return ((const shdcp0_pktlen_t *)l)->pkt->hwaddr - ((const shdcp0_pktlen_t*)r)->pkt->hwaddr;
}

int main(int ac, char *av[])
{
    DF_BT_ENTRY;

    dflog_open(basename(av[0]), DFLOG_STDERR);
    plarray = df_da_create(sizeof(shdcp0_pktlen_t), pl_dtor, pl_cmp);

    if (ac < 2 || ac > 2)
    {
	dflog(LOG_ERR, "parameters: iface | none");
	return EXIT_FAILURE;
    }

    //dflog(LOG_INFO, "start");

    const char *ifname = NULL;
    if (ac == 2)
	ifname = av[1];

    if (ifname != NULL && strcmp(ifname, "none")==0)
	ifname = NULL;

    int sk = shdcp_socket(ifname, 1);

    struct sigaction sigact;
    memset(&sigact, 0, sizeof(sigact));
    sigact.sa_handler = sig_handler;
    sigact.sa_flags   = SA_RESTART;

    signal(SIGCHLD, SIG_IGN);
    signal(SIGPIPE, SIG_IGN);
    signal(SIGHUP , SIG_IGN);
    df_xsigaction(SIGINT , &sigact);
    df_xsigaction(SIGQUIT, &sigact);
    df_xsigaction(SIGTERM, &sigact);


    {   // send discovery requests
	pid_t child = fork();
	if (child == -1)
	    dferror(EXIT_FAILURE, errno, "fork failed");

	if (child == 0)
	{
	    size_t n;
	    for (n = SHDCP0_SEND_REPEAT_COUNT - 1; n; --n)
	    {
		send_discovery_request(sk);
		usleep(SHDCP0_SEND_REPEAT_DELAY_USEC);
	    }
	    send_discovery_request(sk);

	    exit(EXIT_SUCCESS);
	}
    }


    int rc = on_exit(onexit, NULL);
    if (rc != 0)
	dferror(EXIT_FAILURE, errno, "on_exit() failed");

    rc = df_tty_set_char_input_mode(STDIN_FILENO, 0, &saved_tattr);
    if (rc != 0)
	dferror(EXIT_FAILURE, errno, "df_tty_set_char_input_mode() failed");


    print_headers();

    rc = 0;
    struct pollfd pollfd[2] = {
	{ .fd = STDIN_FILENO, .events = POLLIN },
	{ .fd = sk          , .events = POLLIN }
    };

    while ( !stop )
    {
	rc = poll(pollfd, 2, -1);
	if (rc < 0)
	{
	    if (errno == EINTR) // signal received, stop
		continue;

	    dferror(EXIT_FAILURE, errno, "poll failed");
	}
	else if (rc == 0) // timeout
	    dferror(EXIT_FAILURE, errno, "poll timeout");
	else if (rc >= 1 && rc <= 2)
	{
	    if (pollfd[1].revents & POLLIN)
	    {
		int rsize = 0;
		int err = ioctl(sk, FIONREAD, &rsize);
		if (err < 0)
		    dferror(EXIT_SUCCESS, errno, "ioctl FIONREAD failed");
		else // err >= 0
		{
		    if ((size_t)rsize > recv_pktlen.len)
			shdcp0_pktlen_alloc(&recv_pktlen, rsize);

		    struct sockaddr_in src_addr;
		    socklen_t src_addrlen = sizeof(src_addr);
		    ssize_t rrc = recvfrom(sk, recv_pktlen.pkt, recv_pktlen.len,
					   0, &src_addr, &src_addrlen);
		    if (rrc < 0)
			dferror(EXIT_SUCCESS, errno, "recvfrom failed");
		    else
		    {
			if (rrc != rsize)
			{
			    dflog(LOG_ERR,
				  "%d bytes expected but %zd received",
				  rsize, rrc);
			}
			else
			{
#if 0
			    dflog(LOG_INFO,
				  "%zd bytes packet received from %s:%d",
				  rrc,
				  inet_ntoa(src_addr.sin_addr),
				      ntohs(src_addr.sin_port));
#endif
			    process_packet(sk, recv_pktlen.pkt, rrc);
			}
		    }
		}
	    } // pollfd[1].revents & POLLIN
	    else if (pollfd[0].revents & POLLIN)
	    {
		char buf[8];
		ssize_t l = read(STDIN_FILENO, &buf, sizeof(buf));
		if (l < 0)
		    dferror(EXIT_FAILURE, errno, "read stdin failed");
		if (l == 0)
		{
		    dferror(EXIT_FAILURE, errno, "EOF on stdin");
		}
		else
		{
		    //dflog(LOG_INFO, "/%c/ key readed", buf[0]);

		    if (buf[0] == 'c')
		    {
			dflog(LOG_INFO, "\n");
			dflog(LOG_INFO, "Cleaning table");
			df_da_free(plarray);
			print_headers();
		    }

		    if (buf[0] == 'd')
		    {
			dflog(LOG_INFO, "\n");
			dflog(LOG_INFO, "Cleaning table and sending Discovery Request");
			df_da_free(plarray);
			print_headers();
			send_discovery_request(sk);
		    }

		    if (buf[0] == 'h')
		    {
			dflog(LOG_INFO, "\n");
			print_headers();
		    }

		    if (buf[0] == 'q')
		    {
			dflog(LOG_INFO, "\n");
			stop = 1;
		    }
		}
	    } //pollfd[0].revents & POLLIN
	    else
	    {
		dflog(LOG_ERR, "No events on poll return!");
	    }
	}
	else
	    dferror(EXIT_FAILURE, errno, "poll returned %d, 1 expected", rc);
    }

    close(sk);
    dflog(LOG_INFO, "exit");
    dflog_close();
    return EXIT_SUCCESS;
}
